from elements import gameplay_miner as gpm
from data import gameplaystores as gps

# LIST OF SITES STRUCTURED BY OBJECTS:
gameplaystores = gpm.Gameplay_miner(category=gps.categories, url='https://www.gameplaystores.es/')



# START MINING AND SAVE INTO DATABASE:
gameplaystores.recover_list()