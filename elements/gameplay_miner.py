from bs4 import BeautifulSoup
import urllib.request as request
import pandas as pd

class Gameplay_miner():
    """ Recovery all consoles, games, etc from webiste """

    product_list = []

    def __init__(self, category, url):
        self.category = category
        self.url = url

    def get_first_page(self, category):
        """ get a first page and return a number """

        url = self.url + category 

        print('procesando primera página... \n')

        ourUrl = request.urlopen(url)
        soup = BeautifulSoup(ourUrl, 'html.parser')

        # Get number of pages:
        total_pages = [] 
        for i in soup.find('ul',{'class': 'page-list'}).find_all('li'):
            page = i.find('a')
            try:
                total_pages.append(i.find('a').contents[0] \
                                .replace('\n                                                                    ', '') \
                                .replace('\n                                                            ', ''))
            except:
                pass

        print("Paginas a escanear: " + str(total_pages[-2]))
        return int(total_pages[-2])

    def recover_list(self):
        """ Recovery all list and make a dataframe """

        for c in self.category:
            url = self.url + c

            # Recovery first page list:
            print('Recuperando listados de categoría \n')

            ourUrl = request.urlopen(url)
            soup = BeautifulSoup(ourUrl, 'html.parser')

            try:
                page = 1
                games_list = ''
                total_pages = self.get_first_page(c)
                while page <= total_pages:
                    print('recuperando página ' + str(page) + '...\n')
                    games_list = request.urlopen(url + '?page=' + str(page))
                    soup = BeautifulSoup(games_list, 'html.parser')
                    page += 1


                    print('recuperando títulos...\n')
                    for i in soup.find_all('div', {'class': 'js-product-miniature-wrapper'}):
                        set = i.find('h3').find('a')
                        title = set.contents[0]
                        game_url = set.get('href')
                        image = i.find('img', {'class': 'js-lazy-product-image'}).get('data-src')
                        price = i.find('span', {'class': 'product-price'}).get('content')
                        available = False if i.find('span', {'class': 'product-unavailable'}) else True
                        self.product_list.append({'title': title, 
                                            'image': image, 
                                            'url': game_url,
                                            'price': price + ' €',
                                            'system': soup.find('h1', {'class': 'h1 page-title'}).find('span').contents[0],
                                            'available': available}
                                        )

                products_dataframe = pd.DataFrame(self.product_list)
                print(products_dataframe)
            except:
                pass
